package com.gmail.pages;

import com.codeborne.selenide.ElementsCollection;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.CollectionCondition.texts;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class GmailPage {
    public ElementsCollection mails = $$("[role='main'] .zA");

    @Step
    public void sendMail(String email, String topic){
        $(byText("COMPOSE")).click();
        $(byName("to")).val(email);
        $(byName("subjectbox")).val(topic);
        $(byText("Send")).click();
    }

    @Step
    public void assertMails(String ... mailTexts){
        mails.shouldHave(texts(mailTexts));
    }

    @Step
    public void assertMail(int index, String mailText){
        mails.get(index).shouldHave(text(mailText));
    }

    @Step
    public void refresh(){
        $("div[aria-label='Refresh']").click();
    }

    @Step
    public void goToSent(){
        $(byTitle("Sent Mail")).click();
    }

    @Step
    public void search(String text){
        $(byName("q")).val(text).pressEnter();
    }

    @Step
    public void assertMailsListSizeEqual(int number){
        mails.shouldHaveSize(number);
    }

    public void gotToInbox() {
        $(byTitle("Inbox")).click();
    }
}
