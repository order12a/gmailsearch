package com.gmail.pages;

import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.Selenide.$;

public class LoginPage {

    @Step
    public void login(String login, String password){
        $("#Email").val(login).pressEnter();
        $("#Passwd").val(password).pressEnter();
    }
}
