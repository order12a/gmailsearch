package com.gmail.testconfigs;

import com.codeborne.selenide.Screenshots;
import org.junit.Rule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import ru.yandex.qatools.allure.annotations.Attachment;

import java.io.File;
import java.io.IOException;

import static com.google.common.io.Files.toByteArray;

public class BaseTest {

    @Rule
    public TestWatcher screenshotOnFailure = new TestWatcher() {

        @Override
        protected void failed(Throwable e, Description description) {
            try {
                screenshot();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }

        @Attachment(type = "image/png")
        public byte[] screenshot() throws IOException {
            File screenshot = Screenshots.takeScreenShotAsFile();
            return toByteArray(screenshot);
        }
    };
}
