package com.gmail;

import com.codeborne.selenide.Configuration;
import org.junit.BeforeClass;
import org.junit.Test;
import com.gmail.pages.GmailPage;
import com.gmail.pages.LoginPage;
import com.gmail.testconfigs.BaseTest;
import com.gmail.testdata.TestData;

import java.util.Date;

import static com.codeborne.selenide.Selenide.open;

public class GmailTest extends BaseTest{

    @BeforeClass
    public static void config(){
        Configuration.timeout = 20000;
    }

    LoginPage loginPage = new LoginPage();
    GmailPage gmailPage = new GmailPage();

    @Test
    public void testLoginSendReceiveAndSearchOfMail(){
        open("http://gmail.com");

        loginPage.login(TestData.email, TestData.password);

        String mailTopic = "Email " + new Date().getTime();
        gmailPage.sendMail(TestData.email, mailTopic);
        gmailPage.refresh();

        gmailPage.assertMail(0, mailTopic);

        gmailPage.goToSent();

        gmailPage.assertMail(0, mailTopic);

        gmailPage.gotToInbox();
        gmailPage.search(mailTopic);

        gmailPage.assertMails(mailTopic);
    }
}
